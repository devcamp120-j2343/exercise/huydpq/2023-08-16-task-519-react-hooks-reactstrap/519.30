import { useState } from "react";
import BodyInput from "./bodyInput/bodyInput";
import BodyOutput from "./bodyOutput/bodyOutout";

const Body = () => {
    const [inputMessage, setInputMessage] = useState("")
    const [outputMessage, setOutputMessage] = useState([])

    const [likeDisplay, setLikeDisplay] = useState(false)


    const updateInputMessage = (message) => {
        setInputMessage(message)
    }

    const updateOutputMessage = () => {
        if (inputMessage) {
            setOutputMessage([...outputMessage, inputMessage])
            setLikeDisplay(true)
        }
    }
    return (
        <>
            <BodyInput
                inputMessageProp={inputMessage}
                updateInputMessaeProp={updateInputMessage}
                updateOutputMessageProp={updateOutputMessage}
            />
            <BodyOutput outputMessageProp={outputMessage} likeDisplayProp={likeDisplay} />
        </>
    )

}

export default Body