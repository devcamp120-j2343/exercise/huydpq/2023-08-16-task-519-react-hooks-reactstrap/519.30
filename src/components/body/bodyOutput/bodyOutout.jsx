import like from '../../../assets/images/tải xuống.png'


const BodyOutput = ({outputMessageProp, likeDisplayProp }) => {
        return (
            <>
                <div className="mt-4">
                    {
                    outputMessageProp.map( function (element, index){
                        return (
                            <p key={index}> {element}</p>
                        )
                    })
                    }
                    {likeDisplayProp ? <img height="100px" width="100px" src={like} alt="likeImages" /> : null}
                </div>
            </>
        )
}
export default BodyOutput 