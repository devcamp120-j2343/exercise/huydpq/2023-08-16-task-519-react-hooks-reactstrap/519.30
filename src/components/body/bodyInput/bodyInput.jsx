import { Button, Input, Label } from "reactstrap";

const BodyInput = ({inputMessageProp, updateInputMessaeProp, updateOutputMessageProp}) => {
    const change = (event) => {
        console.log(event.target.value)
        updateInputMessaeProp(event.target.value)
    }

    const click = () => {
        console.log('Gửi thông điệp được ấn')
        updateOutputMessageProp()
    }
    // let {inputMessageProp} = this.props
    return (
        <>
            <div className="row mt-4">
                <Label>Message cho bạn 12 tháng tới</Label>
                <Input placeholder="Nhập message của bạn vào đây" className="form-control mt-3" onChange={change} value={inputMessageProp} />
            </div>

            <Button color="success" className="w-25 mt-4" onClick={click}>Gửi thông điệp</Button>

        </>
    )

}

export default BodyInput