import { Component } from "react";
import { Col, Row } from "reactstrap";

import background from "../../../assets/images/landscape-photography_1645.jpg"

class HeaderImage extends Component {
  render() {
    return (
      <Row className="mt-4">
        <Col>
          <img height="300px" width="500px" src={background} alt="ảnh" />
        </Col>
      </Row>
    )
  }
}

export default HeaderImage